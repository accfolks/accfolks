import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedataService } from '../service/sharedata.service';
import { GetPinService } from '../service/get-pin.service';
import { UserdetailsService } from '../service/userdetails.service';
@Component({
  selector: 'app-addressdata',
  templateUrl: './addressdata.component.html',
  styleUrls: ['./addressdata.component.scss'],
  providers: [GetPinService]
})
export class AddressdataComponent implements OnInit {
  //Initializing the variables
  addressForm: FormGroup;
  //to show the required error message
  errortags = ['address1', 'address2', 'zip'];
  //On page load all the address details should be blank
  
  address = { 'village': '', 'town': '', 'district': '', 'state': '', 'country': '' };
  //On page load address fields should be editable
  Isaddressdisabled = null;
  constructor(private fb: FormBuilder, private shareddata: SharedataService, private router: Router, private pincodeservice: GetPinService, private userservice: UserdetailsService) {
    this.addressForm = this.fb.group({
      address1: ['', Validators.required],
      address2: ['', Validators.required],
      zip: ['', Validators.pattern('^[1-9][0-9]{5}$')],
      village: [''],
      town: [''],
      district: [''],
      state: [''],
      country: [''],
    });
    if (this.userservice.loginuserdata) {
      this.address = this.userservice.loginuserdata;
    }
  }

  //On Submit click of address data
  SaveAddress(data) {
    if (data.status === 'INVALID') {
      for (let i = 0; i < this.errortags.length; i++) {
        data.controls[this.errortags[i]].pristine = false;
      }
    }
    else {
      this.shareddata.employeedata.addressdata = data.value;
      let userobj:any = {};
      userobj.email = this.shareddata.employeedata.userdata.email;
      userobj.firstName = this.shareddata.employeedata.userdata.firstname;
      userobj.middleName = this.shareddata.employeedata.userdata.middlename ? this.shareddata.employeedata.userdata.middlename : '';
      userobj.lastName = this.shareddata.employeedata.userdata.lastname;
      userobj.gender = this.shareddata.employeedata.userdata.gender ? this.shareddata.employeedata.userdata.gender : 'Male';
      userobj.dob = this.shareddata.employeedata.userdata.dob;
      userobj.mobile = this.shareddata.employeedata.userdata.mobile;
      userobj.landline = this.shareddata.employeedata.userdata.landline;
      userobj.relation = this.shareddata.employeedata.userdata.relation ? this.shareddata.employeedata.userdata.relation : 'Manager' ;
      userobj.status = this.shareddata.employeedata.userdata.status ? this.shareddata.employeedata.userdata.status : 'TL';
      userobj.image = this.shareddata.employeedata.userdata.image ? this.shareddata.employeedata.userdata.image : null;
      
      userobj.address1 = this.shareddata.employeedata.addressdata.address1;
      userobj.address2 = this.shareddata.employeedata.addressdata.address2;
      userobj.pinCodeId = this.shareddata.employeedata.addressdata.zip;
      userobj.country = this.shareddata.employeedata.addressdata.country;
      userobj.state = this.shareddata.employeedata.addressdata.state;
      userobj.district = this.shareddata.employeedata.addressdata.district;
      userobj.town = this.shareddata.employeedata.addressdata.town;
      userobj.village = this.shareddata.employeedata.addressdata.village;
      if(this.userservice.loginuserdata && this.userservice.loginuserdata.userId){
        //Update user
        userobj.userId = this.userservice.loginuserdata.userId;
        userobj.addressId = this.userservice.loginuserdata.addressId;
        userobj.password = this.userservice.loginuserdata.password;
        userobj.uniqueId = this.userservice.loginuserdata.uniqueId;
        this.userservice.User(userobj, 'PUT')
        .subscribe((jsonData) => {
          this.updateusersuccesful(jsonData);
        },
        err => {
          alert('User cannot be updated!!');
        });
      }
      else{
        //save user
        //call user save service
        userobj.userId = 0;
        userobj.addressId = 0;
        userobj.password = 'test123';
        userobj.uniqueId = Math.floor(1000 + Math.random() * 9000);
        this.userservice.User(userobj, 'POST')
        .subscribe((jsonData) => {
          this.saveusersuccesful(jsonData);
        },
        err => {
          alert('User cannot be saved!!');
        });
      }
    }
  }

  saveusersuccesful(data){
    
    alert('User saved successfully!!. Please Login now to book a meeting.');
    this.router.navigate(['/']);
  }

  updateusersuccesful(data){
    alert('User updated successfully!!. Please Login now to book a meeting.');
    this.router.navigate(['/']);
  }

  //To go back to User form
  BackAddress() {
    this.router.navigate(['/user'])
  }

  //To Get the address details from Pincode using API call
  PinCodeSearch(data) {
    //api call to fetch the address details using service
    if (data.controls.zip._pendingValue) {
      if ((data.controls.zip._pendingValue.toString()).length === 6) {
        this.pincodeservice.getpinDetail(data.controls.zip._pendingValue)
          .subscribe((jsonData) => {
            this.showaddressdetailsbypincode(jsonData);
          },
          err => {
            this.cleardata();
            alert('No details found!!');
          });
      }
      else {
        this.cleardata();
      }
    }
  }

  showaddressdetailsbypincode(data) {
    if (data) {
      this.address.village = data.village;
      this.address.town = data.town;
      this.address.district = data.district;
      this.address.state = data.state;
      this.address.country = data.country;
      this.Isaddressdisabled = true;
    }
    else {
      this.cleardata();
    }
  }

  cleardata() {
    this.address = { 'village': '', 'town': '', 'district': '', 'state': '', 'country': '' };
    this.Isaddressdisabled = null;
  }


  ngOnInit() {

  }


}
