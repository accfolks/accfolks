
import { Form, FormsModule,FormGroup,ReactiveFormsModule,FormControl } from '@angular/forms'
import { DatePipe } from '@angular/common';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Route} from'@angular/router';
import { RouterModule } from '@angular/router';
import { routes } from '../app.route';
import { AddressdataComponent } from './addressdata/addressdata.component';
import { MeetingDataComponentComponent } from './meeting-data-component/meeting-data-component.component';
import {HeaderComponent} from './header/header.component';
import { AppComponent } from './app.component';
import { OnlyalphabetsDirective } from './onlyalphabets.directive';
import { SharedataService } from './service/sharedata.service';

import { FormdataComponent } from './formdata/formdata.component';
import { MeetingServiceService } from './service/meeting-service.service';
import { LoginComponent } from './login/login.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { UserdetailsService } from './service/userdetails.service'
import {FrontLoggedComponent} from './front-logged/front-logged.component'
import {PriorityComponent} from './priority/priority.component'
@NgModule({
  declarations: [
    AppComponent,
  
    OnlyalphabetsDirective,
    MeetingDataComponentComponent,
    AddressdataComponent,
    HeaderComponent,
    FormdataComponent,
 
    LoginComponent,
    FrontLoggedComponent,
    PriorityComponent
    
   
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule, HttpModule,
    HttpClientModule,
    MultiselectDropdownModule,
    FormsModule,
    RouterModule.forRoot(
      routes
    )

  ],
  providers: [SharedataService,DatePipe, UserdetailsService,MeetingServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
