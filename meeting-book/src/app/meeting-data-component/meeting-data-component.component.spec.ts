import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms'
import { DatePipe } from '@angular/common';
import { MeetingDataComponentComponent } from './meeting-data-component.component';
import { Component } from '@angular/compiler/src/core';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { FormBuilder } from '@angular/forms/src/form_builder';

describe('MeetingDataComponentComponent', () => {
  let component: MeetingDataComponentComponent;
  let fixture: ComponentFixture<MeetingDataComponentComponent>;
  let de: DebugElement;
  let el: HTMLElement;
  let value = true;
  let obj;
  let spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MeetingDataComponentComponent],
      imports: [ReactiveFormsModule],
      providers: [DatePipe]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingDataComponentComponent);
    component = fixture.componentInstance;
    //de = fixture.debugElement.query(By.css('li'));
    //el = de.nativeElement;
    fixture.detectChanges();

    //spyOn(component, 'justTesting').and.returnValue(false);
    spyOn(component, 'generateTime').and.returnValue(false);
    //component.justTesting();
    
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be true', () => {
    expect(value).toBeTruthy();
  });

  it('should call', () => {
    component.generateTime();
    //expect(component.generatetime).toHaveBeenCalled();
    expect(component.generateTime).toHaveBeenCalled();
    //expect(component.justTesting).toHaveBeenCalledWith('hii');
    //expect(spy.calls.count()>0).toBeTruthy();
    //expect(component.generatetime()).toHaveBeenCalled();
  });

});
