import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ReactiveFormsModule, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { MeetingServiceService } from '../service/meeting-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserdetailsService } from '../service/userdetails.service';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-meeting-data-component',
  templateUrl: './meeting-data-component.component.html',
  styleUrls: ['./meeting-data-component.component.scss']
})

export class MeetingDataComponentComponent implements OnInit {

  selectedAttendeesId: Object[];
  userList: any[] = [];

  meetingScheduler: string;
  meetingAttendee: string;
  selectedAttendees = [];

  meetingForm: FormGroup;
  formSubmitted = false;
  formData: any = [];
  meetingFormData:any={};

  meetingDate: any;
  meetingStartTime: any;
  todayStartTime: any = '00:00';
  meetingDuration: any;
  duration: any;
  meetEnd: Object = {};

  today = new Date();
  currentDate: any;
  todayDate: String;

  meetingEndTime: String;
  startDate: string;
  startTime: string;

  lessThanCurrentTime = false;
  endTime: number[] = new Array();
  endTimeString: string[] = new Array();

  errorTags = ['meetingDate', 'meetingStartTime', 'meetingDuration', 'meetingEndTime', 'meetingScheduler'];
  meetingAttendeeError = false;
  meetingDurationError = false;

  constructor(private formBuilder: FormBuilder, private datepipe: DatePipe, private meetingService: MeetingServiceService,private loginService:UserdetailsService) {
    this.generateTime();
    this.todayDate = datepipe.transform(this.today, 'yyyy-MM-dd');
    this.todayStartTime = datepipe.transform(this.today, 'HH:mm');
    this.meetingForm = this.formBuilder.group({
      meetingDate: ['', Validators.required],
      meetingStartTime: ['', Validators.required],
      meetingDuration: ['', Validators.required],
      meetingEndTime: ['', Validators.required],
      meetingScheduler: ['', Validators.required],
      meetingAttendee: []
    });
  }

  ngOnInit() {
    this.meetingScheduler = this.loginService.loginuserdata.firstName + ' '+this.loginService.loginuserdata.middleName+' '+this.loginService.loginuserdata.lastName ;
    this.meetingService.getUserIdsAndNames().subscribe(data => {
      for (let i = 0; i < data.length; i++) {
        data[i].name = data[i].firstName + (data[i].middleName === '' ? ' ' : (' ' + data[i].middleName + ' ')) + data[i].lastName;
        data[i].id = data[i].userId;
      }
      this.userList = data;
    });
  }

  //fill dropdown with time slots
  generateTime() {
    for (let i = 0; i <= 300; i = i + 15) {
      if (i !== 0) {
        if (i < 60) {
          let obj: any = {};
          obj.val = i;
          obj.text = i + " min";
          this.endTimeString.push(obj);
        }
        else {
          let hours = Math.floor(i / 60);
          let minutes = i % 60;
          if (minutes == 0) {
            let obj: any = {};
            obj.val = i;
            obj.text = hours + " hour";
            this.endTimeString.push(obj);
          }
          else {
            let obj: any = {};
            obj.val = i;
            obj.text = hours + " hour " + minutes + " minutes";
            this.endTimeString.push(obj);
          }
        }
      }
    }
  }

  //set start time
  startTimeChange(form) {
    if (this.startDate === this.todayDate) {
      if (this.startTime < this.todayStartTime) {
        this.lessThanCurrentTime = true;
      }
      else {
        this.lessThanCurrentTime = false;
      }
    }
    this.setTimeChange(this.duration);
  }

  //set end time using duration
  setTimeChange(event) {
    if (this.startTime && typeof (event) == 'string') {         //when called through startTimeChange
      let minutes1 = parseInt(this.startTime.split(':')[1]) + parseInt(this.duration);
      let hours = 0;
      if (minutes1 >= 60) {
        hours = parseInt(this.startTime.split(':')[0]) + Math.floor(minutes1 / 60);
        minutes1 = minutes1 % 60;
      }
      else {
        hours = parseInt(this.startTime.split(':')[0]);
      }
      if (hours >= 24) {
        hours = Math.abs(24 - hours);
      }

      if (minutes1 < 10) {
        this.meetingEndTime = hours + ":0" + minutes1;
      }
      else {
        this.meetingEndTime = hours + ":" + minutes1;
      }
    }

    else if (this.startTime && typeof (event) != 'string') {        //when called through dropdown
      if (event && event.currentTarget) {
        for (let i = 0; i < event.currentTarget.children.length; i++) {
          if (event.currentTarget.children[i].selected) {
            this.duration = event.currentTarget.children[i].value;
          }
        }

        let minutes2 = parseInt(this.startTime.split(':')[1]) + parseInt(this.duration);
        let hours = 0;
        if (minutes2 >= 60) {
          hours = parseInt(this.startTime.split(':')[0]) + Math.floor(minutes2 / 60);
          minutes2 = minutes2 % 60;
        }
        else {
          hours = parseInt(this.startTime.split(':')[0]);
        }
        if (hours >= 24) {
          hours = Math.abs(24 - hours);
        }
        if (minutes2 < 10) {
          this.meetingEndTime = hours + ":0" + minutes2;
        }
        else {
          this.meetingEndTime = hours + ":" + minutes2;
        }
      }

    }

  }

  //submit function
  saveData(form) {

    if (this.selectedAttendees.length == 0) {
      this.meetingAttendeeError = true;
    }
    else {
      this.meetingAttendeeError = false;
    }
    if (form.status === 'INVALID') {
      for (let i = 0; i < this.errorTags.length; i++) {
        form.controls[this.errorTags[i]].pristine = false;
      }
    }
    else {
      this.formData = [];

      this.meetingFormData={'userId':123,'dateId': 0,'startDate':2017-12-15,'startTime':5450,'endDate':2017-12-15,'endTime':5455,'attendee':124,'isActive': 'true'};
      //this.formData.push(form.value,this.loginService.loginuserdata.userId);
      //this.meetingDate = this.formData[0].meetingDate;
      //this.meetingStartTime = this.formData[0].meetingStartTime;
      //this.meetingDuration = this.formData[0].meetingDuration;
      //this.formData.push(this.meetingService.meetingData.userId);
      //this.formData.push(this.formData[0].meetingEndTime);
      //this.formData.push(this.selectedAttendeesId);
      //this.meetingEndTime // finalendtime
      //this.meetingScheduler // scheduler name
      this.formSubmitted = true;
      this.meetingService.saveMeeting(this.meetingFormData,'POST')
      .subscribe((jsonData) => {
       // this.saveMeeting(this.jsonData,'POST');
      }
      );
      //this.meetingFormData.push();

      // this.meetingService.saveMeeting(form.value,'POST')
      // .subscribe((jsonData) => {
      //   this.(jsonData);
      // },
      // err => {
      //   alert('No users found. Please Sign Up first!!');
      // });
    }
  }

  // Multi Select Dropdown Code 
  onChange(e) {
    if (e && e.length > 0) {
      this.selectedAttendees = [];
      for (let i = 0; i < this.userList.length; i++) {
        for (let j = 0; j < e.length; j++) {
          if (this.userList[i].userId === e[j]) {
            this.selectedAttendees.push(this.userList[i]);
            break;
          }
        }
        if (this.selectedAttendees.length == 0) {
          this.meetingAttendeeError = true;
        }
        else {
          this.meetingAttendeeError = false;
        }
      }
    }
    else {
      this.selectedAttendees = [];
    }
  }

  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'checkboxes',
    buttonClasses: 'btn btn-default btn-block',
    pullRight: false,
    dynamicTitleMaxItems: 0,
    displayAllSelectedText: false,
    searchMaxLimit: 20,
    showUncheckAll: true,
    maxHeight: '250px'
  }

  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'selected',
    checkedPlural: 'selected',
    searchPlaceholder: 'Find',
    searchEmptyResult: 'Nothing found...',
    searchNoRenderText: 'Type in search box to see results...',
    defaultTitle: 'Select',
    allSelected: 'All selected',
  };
}
