import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class GetPinService {
  private pindetaillURL:string = "http://10.195.108.20:8096/user/controller/getPincodeByPincode/";
  constructor(private _http: Http) { }
  getpinDetail(pincode)
  {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json; charset=UTF-8');
    let options = new RequestOptions( {method: RequestMethod.Get, headers: headers });
    return this._http.get(this.pindetaillURL + pincode, options)
          .map((response: Response) => {
            if(response.ok){
            return response.json()
            }
          });
          
          
          
  }
 
}
