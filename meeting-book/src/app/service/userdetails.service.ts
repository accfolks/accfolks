import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class UserdetailsService {
  private loginURL: string = "http://10.195.108.20:8096/user/controller/login";
  private userURL: string = 'http://10.195.108.20:8096/user/controller/'
  loginuserdata: any = {};
  constructor(private _http: Http) { }
  
  loginDetails(request) {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json; charset=UTF-8');
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    return this._http.post(this.loginURL, request, options)
      .map((response: Response) => {
        if (response.ok) {
          return response.json()
        }
      });
  }

  User(request, methodtype) {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json; charset=UTF-8');
    let url = '';
    if (methodtype === 'POST') {
      let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
      url = 'User';
      return this._http.post(this.userURL + url, request, options)
      .map((response: Response) => {
        if (response.ok) {
          return response.json()
        }
      });
    }
    else {
      let options = new RequestOptions({ method: RequestMethod.Put, headers: headers });
      url = 'updateUser';
      return this._http.put(this.userURL + url, request, options)
      .map((response: Response) => {
        if (response.ok) {
          return response.json()
        }
      });
    }
  }

}
