import { TestBed, inject } from '@angular/core/testing';
import { GetPinService } from './get-pin.service';


describe('GetPinService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetPinService]
    });
  });

  it('should be created', inject([GetPinService], (service: GetPinService) => {
    expect(service).toBeTruthy();
  }));
});
