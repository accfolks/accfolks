import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';

@Injectable()
export class MeetingServiceService {
  baseUrl='http://10.195.108.20:8096';
  getAllUsersUrl = '/usercontroller/getAllUsers';
  addMeetingUrl='/user/controller/meeting';

  meetingData: any = {};
  constructor(private http: Http) { }
  getUserIdsAndNames() {
    return this.http.get(this.baseUrl+this.getAllUsersUrl)
      .map((res: Response) => { return res.json(); });
  }

  saveMeeting(request, methodtype) {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json; charset=UTF-8');
    let url = '';
    if (methodtype === 'POST') {
      let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
      return this.http.post(this.baseUrl+this.addMeetingUrl, request, options)
      .map((response: Response) => {
        if (response.ok) {
          return response.json()
        }
      });
    }
  }
}