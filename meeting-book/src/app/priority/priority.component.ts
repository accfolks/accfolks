import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-priority',
  templateUrl: './priority.component.html',
  styleUrls: ['./priority.component.scss']
})
export class PriorityComponent implements OnInit {
  priorityForm: FormGroup;
  constructor(private fb: FormBuilder) { 
  this.priorityForm = this.fb.group({
    startDate: ['', [Validators.required]],
  });
}
  ngOnInit() {
  }

}