import { Component, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedataService } from '../service/sharedata.service';
import { UserdetailsService } from '../service/userdetails.service';
@Component({
  selector: 'app-formdata',
  templateUrl: './formdata.component.html',
  styleUrls: ['./formdata.component.scss']
})


export class FormdataComponent implements OnInit {
  registrationForm: FormGroup;
  fetchdata: any = [];
  errorstags = ['dob', 'firstname', 'middlename', 'lastname', 'email', 'mobile', 'unique', 'landline'];
  showalertmessage = false;
  userdetails:any = {};
  constructor(private fb: FormBuilder, private router: Router, private shareddata: SharedataService, private userservice: UserdetailsService) {
    this.registrationForm = this.fb.group({
      dob: ['', [Validators.required]],
      firstname: ['', [Validators.required, Validators.pattern(/^[a-zA-Z. ]+$/)]],
      middlename: ['', [Validators.pattern(/^[a-zA-Z]+$/)]],
      lastname: ['', [Validators.required, Validators.pattern(/^[a-zA-Z]+$/)]],
      email: ['', [Validators.required, Validators.pattern(/^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/)]],
      mobile: ['', [Validators.required, Validators.pattern('^(?! )[0-9 \-\']+'), Validators.minLength(10)]],
      unique: ['', [Validators.required, Validators.pattern('^(?! )[0-9 \-\']+'), Validators.minLength(12)]],
      // relation: ['', [Validators.pattern(/^[a-zA-Z]+$/)]],
      landline: ['', [Validators.pattern('^(?! )[0-9 \-\']+'), Validators.minLength(6)]],
    });
    
  }

  SaveRegistration(data) {
    if (data.status === 'INVALID') {
      for (let i = 0; i < this.errorstags.length; i++) {
        data.controls[this.errorstags[i]].pristine = false;
      }
    }
    else {
      this.shareddata.employeedata.userdata = data.value;
      this.router.navigate(['/address']);
    }
  }




  ngOnInit() {
    if (this.userservice.loginuserdata) {
      this.userdetails = this.userservice.loginuserdata;
    }
  }

}
