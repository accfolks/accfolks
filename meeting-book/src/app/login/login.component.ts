import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserdetailsService } from '../service/userdetails.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public showPass = true;
  //Initializing the variables
  loginForm: FormGroup;
  //to show the required error message
  errortags = ['username', 'password'];
  constructor(private router: Router, private fb: FormBuilder, private userservice:UserdetailsService) {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.userservice.loginuserdata = [];
  }

  //On Login click
  Login(data) {
    if (data.status === 'INVALID') {
      for (let i = 0; i < this.errortags.length; i++) {
        data.controls[this.errortags[i]].pristine = false;
      }
    }
    else {
      //get details from DB
      this.userservice.loginDetails(data.value)
        .subscribe((jsonData) => {
          this.logindetails(jsonData);
        },
        err => {
          alert('No users found. Please Sign Up first!!');
        });
    }
  }

  // routin to dashboard
  logindetails(data) {
    if(data.success && data.success == false){
      alert('No users found. Please Sign Up first!!');
    }
    else{debugger
      this.userservice.loginuserdata = data;
      this.router.navigate(['/dashboard']);
    }
    
  }

  showPassword() {
    this.showPass = !this.showPass;
  }

  SignUp() {
    this.router.navigate(['/user']);
  }
  ngOnInit() {
  }

}
