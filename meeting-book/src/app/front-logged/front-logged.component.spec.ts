import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontLoggedComponent } from './front-logged.component';

describe('FrontLoggedComponent', () => {
  let component: FrontLoggedComponent;
  let fixture: ComponentFixture<FrontLoggedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontLoggedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontLoggedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
