import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appOnlyalphabets]'
})
export class OnlyalphabetsDirective {

  constructor(private el: ElementRef) { }
  @Input() appOnlyalphabets: boolean;
  @HostListener('keydown', ['$event']) onKeyDown(event) {
    const e = <KeyboardEvent>event;
    if (this.appOnlyalphabets) {
      if ([46, 8, 9, 27, 13, 110, 190].indexOf(e.keyCode) !== -1 ||
        // Allow: Ctrl+A
        (e.keyCode === 65 && e.ctrlKey === true) ||
        // Allow: Ctrl+C
        (e.keyCode === 67 && e.ctrlKey === true) ||
        // Allow: Ctrl+X
        (e.keyCode === 88 && e.ctrlKey === true) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
      }
      // Ensure that it is only alphabets and space
      if (!(e.keyCode >= 65 && e.keyCode <= 120) && (e.keyCode !== 32 && e.keyCode !== 0)) {
        event.preventDefault();
      }
    }
  }

}
