import { Routes } from '@angular/router';
import { FormdataComponent } from './app/formdata/formdata.component';
import { AddressdataComponent } from './app/addressdata/addressdata.component';
import { MeetingDataComponentComponent } from './app/meeting-data-component/meeting-data-component.component';
import {LoginComponent} from './app/login/login.component';
import {FrontLoggedComponent} from './app/front-logged/front-logged.component';
import {PriorityComponent} from './app/priority/priority.component';
export const routes: Routes =[
    {path:'', component:LoginComponent},
    {path:'user', component:FormdataComponent},
   {path: 'address', component: AddressdataComponent},
   {path: 'meeting', component: MeetingDataComponentComponent},
   {path: 'dashboard', component:  FrontLoggedComponent},
   {path:'priority', component: PriorityComponent},
]
